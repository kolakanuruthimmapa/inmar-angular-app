import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

declare var $:any;

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  model: any = {};
  loading = false;
  userdata : any;
  errorMsg : string = '';
  successMsg : string = '';
  currentUser: Object;
  currentUserID: number = 0;
  addCountryForm:any;

  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpClient) {

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser) {
          //console.log(this.currentUser['id']);
          this.currentUserID = this.currentUser['id'];
        }else{
          this.router.navigate(['/login']);
        }

        //Logedin user is not admin
        if (this.currentUser['role'] != 'admin') {
          this.router.navigate(['/user-dashboard']);
        }

        this.addCountryForm = new FormGroup({
          userid : new FormControl(this.currentUserID),
          countryname : new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]+$')]),
          population : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          insured : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          uninsured : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          insuredmoe : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          uninsuredmoe : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          age0to18: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age18to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age21to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age40to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age50to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          income0to138: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to200: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to250: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to400: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income138to400: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          male: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          female: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          whiteNotHispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          blackNotHispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          hispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
        });
    }

  ngOnInit() { }

  saveCountry(){
    //console.log(this.addCountryForm.value);
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        })
      };
      if (this.addCountryForm.valid) {
        console.log(this.addCountryForm.valid);
        let body = JSON.stringify(this.addCountryForm.value);
        this.http.post('http://localhost/examples/inmar/inmar-wordpress/add-country/', body, httpOptions)
                  .subscribe(
                    data => {
                      if (data['status'] != 'success') {
                       //console.log(data); 
                       this.errorMsg = data['message'];
                      }else{
                        console.log(data);
                        this.successMsg = data['message'];
                        //this.addCountryForm.resetForm();
                        this.router.navigate(['/countrylist']);
                      }
                    },
                    error => {
                      console.error(error);//For Error Response
                      this.loading = false;
                    }
                  );
      }else{
        this.errorMsg = 'Fill all the required fields';
      }
  }
}
