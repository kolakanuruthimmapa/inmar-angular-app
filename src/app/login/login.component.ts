import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  	model: any = {};
    loading = false;
    userdata : any;
    errorMsg : string = '';
    currentUser: Object;

    constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
        const localStorageUserID = localStorage.getItem('currentUser');
        if (localStorageUserID && localStorageUserID != 'undefined') {
            this.currentUser = JSON.parse(localStorageUserID);     
            if (this.currentUser) {
              if (this.currentUser['role'] === 'admin') {
                this.router.navigate(['/']);
              } else {
                this.router.navigate(['/user-dashboard']);
              }
            }
        }
    }
  	
  	ngOnInit() {
  	
  	}

  	login(formVal:any) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        })
      };
      let body = JSON.stringify(formVal);
      this.http.post('http://localhost/examples/inmar/inmar-wordpress/login/', body, httpOptions)
                .subscribe(
                  data => {
                    if (data['status'] != 'success') {
                     //console.log(data); 
                     this.errorMsg = data['message'];
                    }else{
                      //console.log(data['userinfo']);
                      this.currentUser = data['userinfo'];
                      localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                      if (this.currentUser['role'] === 'admin') {
                        this.router.navigate(['/']);
                      } else {
                        this.router.navigate(['/user-dashboard']);
                      }
                      //this.router.navigate(['/']);
                    }
                  },
                  error => {
                    console.error(error);//For Error Response
                    this.loading = false;
                  }
                );
    }

}
