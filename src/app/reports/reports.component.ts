import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
	loading: boolean = true;
	errorMsg : string ='';
	successMsg : string = '';

	currentUser: Object;
	currentUserID: number = 0;
	headMapData : any;

	max : Array<any>;

	httpOptions : Object = {
		headers: new HttpHeaders({
		  'Content-Type':  'application/json',
		})
	};
  	
  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
	    if (this.currentUser) {
	      this.currentUserID = this.currentUser['id'];
	    }else{
	      this.router.navigate(['/login']);
	    }

	    //Logedin user is not admin
	    if (this.currentUser['role'] != 'admin') {
	      this.router.navigate(['/user-dashboard']);
	    }
  	}

  	ngOnInit() {
  	  let body = JSON.stringify({
	      userid : this.currentUserID,
	      type : 'heatmap'
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/reports/', body, this.httpOptions)
	    .subscribe(
	      data => {
	        if (data['status'] != 'success') {
	         this.errorMsg = data['message'];
	         this.loading=false
	        }else{
	          this.headMapData = data['info'];
	          this.loading=false
	        }
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
  	}

  	ngAfterViewInit(){
  		/*$(document).ready(function(){
	      // Function to get the Max value in Array
	      this.max = function( array ){
	        return Math.max.apply( Math, array );//Return maximum number
	      };

	      // get all values
	      var counts= $('#heat-map-3 tbody td').not('.stats-title').map(function() {
	        return parseInt($(this).text());
	      }).get();
	      
	      // return max value
	      var max = this.max(counts);

	      let xr = 255;
	      let xg = 255;
	      let xb = 255;

	      let yr = 250;
	      let yg = 237;
	      let yb = 37;

	      let n = 100;
	      
	      // add classes to cells based on nearest 10 value
	      $('#heat-map-3 tbody td').not('.stats-title').each(function(){
	        var val = parseInt($(this).text());
	        //console.log(val+'-'+max+'+'+(val/max)+'-'+Math.round((val/max)) );
	        var pos = parseInt((Math.round((val/max)*100)).toFixed(0));
	        //console.log(Math.round((val/max)*100) );
	        let red = parseInt( ( xr + ( ( pos * (yr - xr) ) / (n-1) ) ).toFixed(0) );
	        let green = parseInt((xg + (( pos * (yg - xg)) / (n-1))).toFixed(0));
	        let blue = parseInt((xb + (( pos * (yb - xb)) / (n-1))).toFixed(0));
	        let clr = 'rgb('+red+','+green+','+blue+')';
	        $(this).css({backgroundColor:clr});
	      });
	    });*/
  	}
}
