import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.css']
})
export class AfterLoginComponent implements OnInit {
	currentUser : number = 0;
	currentUserID : number = 0;
	constructor() {
		const localStorageUserID = localStorage.getItem('currentUser');
		if (localStorageUserID && localStorageUserID != 'undefined') {
			this.currentUser = JSON.parse(localStorageUserID); 
			this.currentUserID = this.currentUser['id'];    
		}		
	}

	ngOnInit() {
	}

}
