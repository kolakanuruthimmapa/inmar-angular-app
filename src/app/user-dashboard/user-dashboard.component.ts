import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
	loading: boolean = true;
	errorMsg : boolean =false;
	successMsg : boolean = false;

	currentUser: Object;
	currentUserID: number = 0;
	currentUserCountryID: number = 0;
	dashboardstats: any;
	countryinfo : any;

	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser) {
          //console.log(this.currentUser);"subscriber"
          this.currentUserID = this.currentUser['id'];
        }else{
          this.router.navigate(['/login']);
        }

        this.currentUserCountryID = this.currentUser['user_country_id'];
        console.log(this.currentUserCountryID);
        this.loading= false;
	}

	ngOnInit() {
		const httpOptions = {
	      headers: new HttpHeaders({
	        'Content-Type':  'application/json',
	      })
	    };

	    let body = JSON.stringify({
	      userid : this.currentUserID,
	      countryid : this.currentUserCountryID,
	      type : 'userdashboard'
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/admin-data/', body, httpOptions)
	    .subscribe(
	      data => {
	        if (data['status'] != 'success') {
	         this.errorMsg = data['message'];
	        }else{
	          if (data['info']) {
	          	this.countryinfo = data['info'];	
	          }
	          if (data['stats']) {
	          	this.dashboardstats = data['stats'];	
	          }
	          this.successMsg = data['message'];
	        }
	        this.loading=false
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
	}

}
