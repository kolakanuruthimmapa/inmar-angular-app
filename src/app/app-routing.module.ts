import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { InsuredComponent } from './insured/insured.component';
import { UninsuredComponent } from './uninsured/uninsured.component';
import { CountrylistComponent } from './countrylist/countrylist.component';
import { ReportsComponent } from './reports/reports.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { ViewcountryComponent } from './countrylist/viewcountry/viewcountry.component';
import { EditCountryComponent } from './countrylist/edit-country/edit-country.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { MyAccountComponent } from './my-account/my-account.component';
const routes: Routes = [
	{
		path : '',
		component : DashboardComponent
	},
	{
		path : 'login',
		component : LoginComponent
	},
	{
		path : 'logout',
		component : LogoutComponent
	},
	{
		path : 'register',
		component : RegisterComponent
	},
	{
		path : 'my-account/:uid',
		component : MyAccountComponent
	},
	{
		path : 'user-dashboard',
		component : UserDashboardComponent
	},
	{
		path : 'insured',
		component : InsuredComponent
	},
	{
		path : 'uninsured',
		component : UninsuredComponent
	},
	{
		path : 'countrylist',
		component : CountrylistComponent
	},
	{
		path : 'reports',
		component : ReportsComponent
	},
	{
		path : 'add-country',
		component : AddCountryComponent
	},
	{
		path : 'country/:id',
		component : ViewcountryComponent
	},
	{
		path : 'edit-country/:id',
		component : EditCountryComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
