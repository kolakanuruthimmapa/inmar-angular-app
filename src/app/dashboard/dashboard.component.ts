import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import * as _ from 'underscore';
import { PagerService } from '../_services/index';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	loading: boolean = true;
	errorMsg : boolean =false;
	successMsg : boolean = false;

	currentUser: Object;
	currentUserID: number = 0;
	countries: any;
	dashboardstats: any;

	// pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private pagerService: PagerService) {
  		const localStorageUserID = localStorage.getItem('currentUser');
  		if (localStorageUserID && localStorageUserID != 'undefined') {
	  		this.currentUser = JSON.parse(localStorageUserID);
		    if (this.currentUser) {
				this.currentUserID = this.currentUser['id'];
				if (this.currentUser['role'] === 'admin') {
					this.router.navigate(['/']);
				} else {
					this.router.navigate(['/user-dashboard']);
				}
		    }else{
		      this.router.navigate(['/login']);
		    }
		}else{
			this.router.navigate(['/login']);
		}
  	}

  	ngOnInit() {
  		const httpOptions = {
	      headers: new HttpHeaders({
	        'Content-Type':  'application/json',
	      })
	    };

	    let body = JSON.stringify({
	      userid : this.currentUserID,
	      type : 'dashboard'
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/admin-data/', body, httpOptions)
	    .subscribe(
	      data => {
	        if (data['status'] != 'success') {
	         this.errorMsg = data['message'];
	        }else{	          
	          if (data['info']) {
	          	this.countries = data['info'];	
	          }
	          if (data['stats']) {
	          	this.dashboardstats = data['stats'];	
	          }
	          // initialize to page 1
                this.setPage(1);
	          this.successMsg = data['message'];
	        }
	        this.loading=false
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
  	}

  	setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.pagerService.getPager(this.countries.length, page);

        // get current page of items
        this.pagedItems = this.countries.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
