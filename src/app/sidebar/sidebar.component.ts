import { Component, OnInit } from '@angular/core';
import {LocationStrategy} from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
	currentUrl : string;
	currentUserRole : string;
	countryActive:boolean;
	currentUser:Object;
	constructor(private url:LocationStrategy) {
		this.currentUrl = window.location.pathname;

		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.currentUserRole = this.currentUser['role'];
	}


	ngOnInit() {
		if (this.url.path().indexOf('/edit-country') > -1 || this.url.path().indexOf('/countrylist') > -1 || this.url.path().indexOf('/add-country') > -1 || this.url.path().indexOf('/country') > -1) {
        	this.countryActive=true;
        }
	}

}
