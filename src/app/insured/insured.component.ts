import { Component, OnInit } from '@angular/core';

import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-insured',
  templateUrl: './insured.component.html',
  styleUrls: ['./insured.component.css']
})
export class InsuredComponent implements OnInit {
	loading: boolean = true;
	errorMsg : string ='';
	successMsg : string = '';

	currentUser: Object;
	currentUserID: number = 0;
	insuredInfo: any;
	ageCatInfo:any;
	sexCatInfo:any;
	incomeCatInfo:any;
	reCatInfo:any;

	httpOptions : Object = {
		headers: new HttpHeaders({
		  'Content-Type':  'application/json',
		})
	};

  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
	  	this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
	    if (this.currentUser) {
	      //console.log(this.currentUser['id']);
	      this.currentUserID = this.currentUser['id'];
	    }else{
	      this.router.navigate(['/login']);
	    }

	    //Logedin user is not admin
	    if (this.currentUser['role'] != 'admin') {
	      this.router.navigate(['/user-dashboard']);
	    }
	    
	    //console.log(this.currentUserID);
	    let body = JSON.stringify({
	      userid : this.currentUserID,
	      type : 'insured'
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/admin-data/', body, this.httpOptions)
	    .subscribe(
	      data => {
	      	//console.log(data);
	        if (data['status'] != 'success') {
	         this.errorMsg = data['message'];
	         this.loading=false
	        }else{
	          this.insuredInfo = data['info'];
	          if (data['info']['age']) {
	          	this.ageCatInfo = data['info']['age'];
	          }
	          if (data['info']['sex']) {
	          	this.sexCatInfo = data['info']['sex'];
	          }
	          if (data['info']['races_ethnicities']) {
	          	this.reCatInfo = data['info']['races_ethnicities'];
	          }
	          if (data['info']['income']) {
	          	this.incomeCatInfo = data['info']['income'];
	          }
	          //console.log(data['info']['age']);
	          this.loading=false
	        }
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );

  	}

  ngOnInit() {
  }
}
