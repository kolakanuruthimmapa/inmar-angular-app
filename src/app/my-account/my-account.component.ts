import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
	loading: boolean = true;
	errorMsg : string = '';
	successMsg : string = '';

	currentUser: Object;
	currentUserID: number = 0;
	returnUserObj: any;
	dashboardstats: any;
  	formData : any;
  	actionType:string='edit';
  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  		const localStorageUserObj = localStorage.getItem('currentUser')
		if (localStorageUserObj && localStorageUserObj != 'undefined') {
			this.currentUser = JSON.parse(localStorageUserObj);
		    if (this.currentUser) {
		      //console.log(this.currentUser);
		      this.currentUserID = this.currentUser['id'];
		    }else{
		      this.router.navigate(['/login']);
		    }
		}

		this.formData = new FormGroup({
			type: new FormControl(this.actionType),
			userid: new FormControl(this.currentUserID),
			fullname : new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(40), Validators.pattern('^[a-zA-Z ]+$'), , this.checkEmpty]),
			phone : new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(14), this.checkEmpty, this.numberValidator]),
			city : new FormControl('', [Validators.required, this.checkEmpty]),
			state : new FormControl('', [Validators.required, this.checkEmpty]),
			pincode : new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), this.checkEmpty, this.numberValidator]),
        });
  	}

  	ngOnInit() {
  		const httpOptions = {
	      headers: new HttpHeaders({
	        'Content-Type':  'application/json',
	      })
	    };

	    let body = JSON.stringify({
	      userid : this.currentUserID,
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/get-user-info/', body, httpOptions)
	    .subscribe(
	      data => {
	      	console.log(data);
	        if (data['status'] == 'success') {
	          this.returnUserObj = data;
	        }
	        this.loading=false
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
  	}

  	emailValidator(control: FormControl) {
        if (control.value && control.value.trim()!="" && !control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return { 'invalidEmailAddress': true };
        }
    }

    checkEmpty(control: FormControl): any {
        if (control.value && control.value.trim().length<1) {
            return { 'emptyValue': true };
        } 
    }

    numberValidator(control: FormControl) {
        if (control.value && ((typeof control.value == 'string' && control.value.trim()!="" && !control.value.trim().match(/^[0]*[1-9][\d]*$/)) || (isNaN(control.value) || control.value<1))) {
            return { 'invalidNumber': true };
        }
    }

    updateAccount(formName:any){
    	if (this.formData.valid) {
	  		this.errorMsg = '';
	    	this.successMsg = '';
	  		
	      	const httpOptions = {
		        headers: new HttpHeaders({
		          'Content-Type':  'application/json',
		        })
		      };
	      	let body = JSON.stringify(this.formData.value);
	      	this.http.post('http://localhost/examples/inmar/inmar-wordpress/get-user-info/', body, httpOptions)
	                .subscribe(
	                  data => {
	                    if (data['status'] != 'success') {
	                     this.errorMsg = data['message'];
	                    }else{
	                    	this.successMsg = data['message'];
	                    }
	                  },
	                  error => {
	                    console.error(error);//For Error Response
	                    this.loading = false;
	                  }
	                );
	    }
    }
}
