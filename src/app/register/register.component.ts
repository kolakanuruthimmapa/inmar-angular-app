import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	model: any = {};
	loading = false;
    userdata : any;
    errorMsg : string = '';
    successMsg : string = '';
    currentUser: Object;
	currentUserID: number = 0;
	formData:any;
	countryExist:boolean=false;
	userExist:boolean=false;

  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  		if (this.currentUser) {
          if (this.currentUser['role'] === 'admin') {
            this.router.navigate(['/']);
          } else {
            this.router.navigate(['/user-dashboard']);
          }
        }

        this.formData = new FormGroup({
          fullname : new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(40), Validators.pattern('^[a-zA-Z ]+$'), , this.checkEmpty]),
          username : new FormControl('', [Validators.required, this.emailValidator, this.checkEmpty]),
          password : new FormControl('', [Validators.required, this.checkEmpty]),
          phone : new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(14), this.checkEmpty, this.numberValidator]),
          city : new FormControl('', [Validators.required, this.checkEmpty]),
          state : new FormControl('', [Validators.required, this.checkEmpty]),
          country : new FormControl('', [Validators.required, this.checkEmpty]),
          pincode : new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), this.checkEmpty, this.numberValidator]),
        });
  	}

  	ngOnInit() {
  	}

  	emailValidator(control: FormControl) {
        if (control.value && control.value.trim()!="" && !control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return { 'invalidEmailAddress': true };
        }
    }

    checkEmpty(control: FormControl): any {
        if (control.value && control.value.trim().length<1) {
            return { 'emptyValue': true };
        } 
    }

    numberValidator(control: FormControl) {
        if (control.value && ((typeof control.value == 'string' && control.value.trim()!="" && !control.value.trim().match(/^[0]*[1-9][\d]*$/)) || (isNaN(control.value) || control.value<1))) {
            return { 'invalidNumber': true };
        }
    }

  	register(formName:any) {
  		if (this.formData.valid) {
        this.loading=true;
	  		this.errorMsg = '';
	    	this.successMsg = '';
	  		console.log(this.formData.value);
	      	const httpOptions = {
		        headers: new HttpHeaders({
		          'Content-Type':  'application/json',
		        })
		      };
	      	let body = JSON.stringify(this.formData.value);
	      	this.http.post('http://localhost/examples/inmar/inmar-wordpress/register/', body, httpOptions)
	                .subscribe(
	                  data => {
	                    if (data['status'] != 'success') {
	                     console.log(data); 
	                     this.errorMsg = data['message'];
	                    }else{
	                    	this.successMsg = data['message'];
	                      //console.log(data['userinfo']);
	                      //this.router.navigate(['/']);
                        formName.resetForm();
	                    }
                      this.loading = false;
	                  },
	                  error => {
	                    console.error(error);//For Error Response
	                    this.loading = false;
	                  }
	                );
	    }
    }

}
