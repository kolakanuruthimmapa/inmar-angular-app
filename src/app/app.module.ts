import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AfterLoginComponent } from './after-login/after-login.component';
import { InsuredComponent } from './insured/insured.component';
import { UninsuredComponent } from './uninsured/uninsured.component';
import { CountrylistComponent } from './countrylist/countrylist.component';
import { ReportsComponent } from './reports/reports.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { ViewcountryComponent } from './countrylist/viewcountry/viewcountry.component';
import { EditCountryComponent } from './countrylist/edit-country/edit-country.component';
import { PagerService } from './_services/index';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { MyAccountComponent } from './my-account/my-account.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UserDashboardComponent,
    SidebarComponent,
    AfterLoginComponent,
    InsuredComponent,
    UninsuredComponent,
    CountrylistComponent,
    ReportsComponent,
    AddCountryComponent,
    ViewcountryComponent,
    EditCountryComponent,
    RegisterComponent,
    LogoutComponent,
    MyAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    PagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
