import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UninsuredComponent } from './uninsured.component';

describe('UninsuredComponent', () => {
  let component: UninsuredComponent;
  let fixture: ComponentFixture<UninsuredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UninsuredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UninsuredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
