import { Component, OnInit } from '@angular/core';

import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import * as _ from 'underscore';
import { PagerService } from '../_services/index';

@Component({
  selector: 'app-countrylist',
  templateUrl: './countrylist.component.html',
  styleUrls: ['./countrylist.component.css']
})
export class CountrylistComponent implements OnInit {
  loading: boolean = true;
  errorMsg : string ='';
  successMsg : string = '';

  currentUser: Object;
  currentUserID: number = 0;
  countries: any;
  httpOptions : Object = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private pagerService: PagerService) {

  	this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      //console.log(this.currentUser['id']);
      this.currentUserID = this.currentUser['id'];
    }else{
      this.router.navigate(['/login']);
    }

    //Logedin user is not admin
    if (this.currentUser['role'] != 'admin') {
      this.router.navigate(['/user-dashboard']);
    }

  }

  ngOnInit() {
    //console.log(this.currentUserID);
    let body = JSON.stringify({
      userid : this.currentUserID
    });
    this.http.post('http://localhost/examples/inmar/inmar-wordpress/get-country-list/', body, this.httpOptions)
    .subscribe(
      data => {
        if (data['status'] != 'success') {
         //this.errorMsg = data['message'];
         this.loading=false
        }else{
          this.countries = data['countries'];
          // initialize to page 1
           this.setPage(1);
          //this.successMsg = data['message'];
          this.loading=false
        }
      },
      error => {
        console.error(error);//For Error Response
        this.loading=false
      }
    );
  }

  setPage(page: number) {
      if (page < 1 || page > this.pager.totalPages) {
          return;
      }

      // get pager object from service
      this.pager = this.pagerService.getPager(this.countries.length, page);

      // get current page of items
      this.pagedItems = this.countries.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  deleteCountry(event:any, countryID:number, indexVal:any){
    event.preventDefault();
    
    var result = confirm("Want to delete?");
    if (result) {
      //console.log(this.currentUserID);
      let body = JSON.stringify({
        userid : this.currentUserID,
        countryid : countryID
      });
      console.log(countryID);
      this.http.post('http://localhost/examples/inmar/inmar-wordpress/delete-country/', body, this.httpOptions)
        .subscribe(
          data => {
            if (data['status'] != 'success') {
             this.errorMsg = data['message'];
             this.loading=false
            }else{
              //console.log(data);
              alert(data['message']);
              this.pagedItems = _.filter(this.pagedItems, (elem)=>elem!=indexVal);
              this.successMsg = data['message'];
              this.loading=false
            }
          },
          error => {
            console.error(error);//For Error Response
            this.loading=false
          }
        );
    }
    
  }
}
