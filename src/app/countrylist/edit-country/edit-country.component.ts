import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.css']
})
export class EditCountryComponent implements OnInit {
	loading = false;
	userdata : any;
	errorMsg : string = '';
	successMsg : string = '';
	currentUser: Object;
	currentUserID: number = 0;

	currentCountryID: number = 0;
	countryInfo: any;

	addCountryForm:any;

	ageCatgory: any;
	incomeCatgory: any;
	sexCatgory: any;
	racesEthnicitiesCatgory: any;
  nopermission:boolean = false;

  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpClient) {

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser) {
          //console.log(this.currentUser['id']);
          this.currentUserID = this.currentUser['id'];
        }else{
          this.router.navigate(['/login']);
        }

        this.route.params.subscribe(params => {
	  		this.currentCountryID = params['id'];
	  	});


        this.addCountryForm = new FormGroup({
        	countryid: new FormControl(this.currentCountryID),
          userid : new FormControl(this.currentUserID),
          countryname : new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]+$')]),
          population : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          insured : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          uninsured : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          insuredmoe : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          uninsuredmoe : new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
          age0to18: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age18to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age21to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age40to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          age50to64: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          income0to138: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to200: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to250: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income0to400: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          income138to400: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          male: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          female: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),

          whiteNotHispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          blackNotHispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
          hispanic: new FormGroup({
            population: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            insured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]), 
            insuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsured: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
            uninsuredMoe: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
           }),
        });
	


		//Get country imformation
		const httpOptions = {
	      headers: new HttpHeaders({
	        'Content-Type':  'application/json',
	      })
	    };

	    let body = JSON.stringify({
	      userid : this.currentUserID,
	      countryid : this.currentCountryID,
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/get-country-info/', body, httpOptions)
	    .subscribe(
	      data => {
	      	console.log(data);
	        if (data['status'] != 'success') {
            if (data['message'] == 'nopermission') {
              this.nopermission = true;
            }else{
	           this.errorMsg = data['message'];
           }
	        }else{	          
	          if (data['info']) {
	          	this.countryInfo = data['info'];
	          }
	          if (data['age']) {
	          	this.ageCatgory = data['age'];	
	          }
	          if (data['income']) {
	          	this.incomeCatgory = data['income'];	
	          }
	          if (data['races']) {
	          	this.racesEthnicitiesCatgory = data['races'];	
	          }
	          if (data['sex']) {
	          	this.sexCatgory = data['sex'];	
	          }
	          
	          this.successMsg = data['message'];
	        }
	        this.loading=false
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
    }

  	ngOnInit() {
  	}

  	updateCountry(){
	    //console.log(this.addCountryForm.value);
	    const httpOptions = {
	        headers: new HttpHeaders({
	          'Content-Type':  'application/json',
	        })
	    };
	    if (this.addCountryForm.valid) {
	        
	        let body = JSON.stringify(this.addCountryForm.value);
	        this.http.post('http://localhost/examples/inmar/inmar-wordpress/add-country/', body, httpOptions)
	                  .subscribe(
	                    data => {
	                    	console.log(data);
          							if (data['status'] != 'success') {
          								this.errorMsg = data['message'];
          							}else{
          								console.log(data);
          								this.successMsg = data['message'];
          								//this.addCountryForm.resetForm();
                          if (this.currentUser['role'] == 'admin') {
                            this.router.navigate(['/countrylist']);
                          }else{
                            this.router.navigate(['/user-dashboard']);
                          }
          								
          							}
	                    },
	                    error => {
	                      console.error(error);//For Error Response
	                      this.loading = false;
	                    }
	                  );
	    }else{
	        this.errorMsg = 'Fill all the required fields';
	    }
  	}
}
