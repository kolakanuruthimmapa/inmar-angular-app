import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewcountry',
  templateUrl: './viewcountry.component.html',
  styleUrls: ['./viewcountry.component.css']
})
export class ViewcountryComponent implements OnInit {
	loading: boolean = true;
	errorMsg : boolean =false;
	successMsg : boolean = false;

	currentUser: Object;
	currentUserID: number = 0;
	currentCountryID: number = 0;
	countryInfo: any;
	ageCatgory: any;
	incomeCatgory: any;
	sexCatgory: any;
	racesEthnicitiesCatgory: any;

  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
	    if (this.currentUser) {
	      //console.log(this.currentUser['id']);
	      this.currentUserID = this.currentUser['id'];
	    }else{
	      this.router.navigate(['/login']);
	    }

	    //Logedin user is not admin
        if (this.currentUser['role'] != 'admin') {
          this.router.navigate(['/user-dashboard']);
        }

	    this.route.params.subscribe(params => {
	  		//console.log("params: ", params);
	  		//console.log("params: ", params['id']);
	  		this.currentCountryID = params['id'];
	  	});

  		const httpOptions = {
	      headers: new HttpHeaders({
	        'Content-Type':  'application/json',
	      })
	    };

	    let body = JSON.stringify({
	      userid : this.currentUserID,
	      countryid : this.currentCountryID,
	    });
	    this.http.post('http://localhost/examples/inmar/inmar-wordpress/get-country-info/', body, httpOptions)
	    .subscribe(
	      data => {
	      	//console.log(data);
	        if (data['status'] != 'success') {
	         this.errorMsg = data['message'];
	        }else{	          
	          if (data['info']) {
	          	this.countryInfo = data['info'];	
	          	//console.log(this.countryInfo);
	          }
	          if (data['age']) {
	          	this.ageCatgory = data['age'];	
	          	//console.log(this.ageCatgory);
	          }
	          if (data['income']) {
	          	this.incomeCatgory = data['income'];	
	          }
	          if (data['races']) {
	          	this.racesEthnicitiesCatgory = data['races'];	
	          }
	          if (data['sex']) {
	          	this.sexCatgory = data['sex'];	
	          }
	          
	          this.successMsg = data['message'];
	        }
	        this.loading=false
	      },
	      error => {
	        console.error(error);//For Error Response
	        this.loading=false
	      }
	    );
  	}

  	ngOnInit() {
  		
  	}

}
