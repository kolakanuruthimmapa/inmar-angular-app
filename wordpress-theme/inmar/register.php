<?php /* Template Name: Register */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();

	$fullname = $_POST['fullname'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$countryname = $_POST['country'];
	$pincode = $_POST['pincode'];
	$phone = $_POST['phone'];


	//Check is there any user with this email id
	if ( email_exists( $username ) ) {	    
	    echo wp_send_json(array('status' => 'error', 'message' => 'Email address already exist. Please choose another'));
	    exit;
	}
	//Check if country is existe with this name
	$exist_country_id = $wpdb->get_var( "SELECT id FROM country_info WHERE name LIKE '%$countryname%'" );
	if (isset($exist_country_id) && !empty($exist_country_id)) {
		$response['message'] = 'Country name is already exist. Please choose another';
		$response['status'] = 'error';
		echo wp_send_json($response);exit;
	}
    //echo wp_send_json($_POST);

	//Create user
	$userdata = array(
	    'user_login'  =>  $username,
	    'user_pass'   =>  $password,
	    'user_email'  =>  $username,
	    'first_name' => $fullname	    
	);

	$user_id = wp_insert_user( $userdata ) ;

	//On success
	if ( ! is_wp_error( $user_id ) ) {
	    //echo "User created : ". $user_id;
	    //Insert country into country_info table
		//If there is information for country after user login redirect to edit country page
	    $wpdb->insert( 
	        'country_info', 
	        array( 
	            'name' => $countryname,
	            'user_id' => $user_id,
	            /*'population' => 0,
	            'insured' => 0,
	            'insured_moe' => 0,
	            'uninsured'	=> 0,
	            'uninsured_moe	' => 0*/
	        ), 
	        array( 
	            '%s', '%d'
	        ) 
	    );
	    $lastid = $wpdb->insert_id;
	    add_user_meta($user_id, 'city', $city);
	    add_user_meta($user_id, 'state', $state);
	    add_user_meta($user_id, 'country', $country);
	    add_user_meta($user_id, 'pincode', $pincode);
	    add_user_meta($user_id, 'phone', $phone);

	    //Default values
	    $default_values = array(
	    	'age_catgory' => array(
				array('category' => 'age0to18', 'category_label'=> '0 to 18'),
				array('category' => "age18to64", 'category_label'=> '18 to 64'),
				array('category' => 'age21to64', 'category_label'=> '21 to 64'),
				array('category' => 'age40to64', 'category_label'=> '40 to 64'),
				array('category' => 'age50to64', 'category_label'=> '50 to 64'),
			),
			'income_catgory' => array(
				array('category' => 'income0to138', 'category_label'=> '0 to 138'),
				array('category' => "income0to200", 'category_label'=> '0 to 200'),
				array('category' => 'income0to250', 'category_label'=> '0 to 250'),
				array('category' => 'income0to400', 'category_label'=> '0 to 400'),
				array('category' => 'income138to400', 'category_label'=> '138 to 400'),
			),
			'sex_catgory' => array(
				array('category' => 'male', 'category_label'=> 'Male'),
				array('category' => "female", 'category_label'=> 'Female')
			),
			'races_ethnicities_catgory' => array(
				array('category' => "whiteNotHispanic", 'category_label'=> "White Not Hispanic"),
				array('category' => "blackNotHispanic", 'category_label'=> "blackNotHispanic"),
				array('category' => "hispanic", 'category_label'=> "Hispanic"),
			),
	    );

		foreach ($default_values as $table_name => $table_values) {
			foreach ($table_values as $column_name => $column_value) {
				$wpdb->insert( 
	                $table_name, 
	                array( 
	                    'country_id' => $lastid,
	                    'category' => $column_value['category'],
	                    'category_label' => $column_value['category_label']
	                    /*'population' => 0,
	                    'insured' => 0,
	                    'insured_moe' => 0,
	                    'uninsured' => 0,
	                    'uninsured_moe' => 0,*/
	                ), 
	                array( 
	                    '%d', '%s', '%s'
	                ) 
	            );
			}
		}

	    $response['message'] = 'User created';
		$response['status'] = 'success';
	}else{
		$response['message'] = 'Somthing went wrong please try again later';
		$response['status'] = 'error';
	}
	echo wp_send_json($response);
}