<?php /* Template Name: Delete Country */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();
	$userid = $_POST['userid'];
	$countryid = $_POST['countryid'];
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id not exists';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	$role = implode(', ', $user_info->roles);
	if ($role != 'administrator') {
		$response['message'] = 'You can not add the country';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	global $wpdb;
	//Check if country exists
	$results = $wpdb->get_results( "SELECT * FROM country_info WHERE id=$countryid", ARRAY_A );
	if (isset($results) && !empty($results)) {
		//Delete country and also delete country categories information
		$wpdb->delete( 'country_info', array( 'id' => $countryid ) );
		$cat_array = array('age' => 'age_catgory', 'income' => 'income_catgory', 'races' => 'races_ethnicities_catgory', 'sex' => 'sex_catgory');
		foreach ($cat_array as $key => $value) {
			$wpdb->delete( $value, array( 'country_id' => $countryid ) );
		}
		
		$response['message'] = 'Country deleted';
		$response['status'] = 'success';
		echo wp_send_json($response);
	}else{
		$response['message'] = 'No country exists with this id';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}
}