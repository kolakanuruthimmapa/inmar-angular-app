<?php 
/*
 Template Name: Upload data
*/
exit;
require('/var/www/html/examples/inmar/inmar-wordpress/wp-content/themes/inmar/XLSXReader-master/XLSXReader.php');
$xlsx = new XLSXReader('/var/www/html/examples/inmar/inmar-wordpress/wp-content/themes/inmar/XLSXReader-master/sample.xlsx');
$sheetNames = $xlsx->getSheetNames();
global $wpdb;
foreach($sheetNames as $sheetName) {
    $sheet = $xlsx->getSheet($sheetName);
    ?>
    <h3><?php echo $sheetName;?></h3>
    <?php 
    $data = $sheet->getData();
    //var_dump($data);
    $i=1;
    foreach ($data as $key => $value) {
        //var_dump($value);
        if ($i==1) {
            $i++;
            continue;
        }
        //Insert country infrmation
        $wpdb->insert( 
            'country_info', 
            array( 
                'name' => $value[0], 
                'population' => $value[1],
                'insured' => $value[2],
                'insured_moe' => $value[3],
                'uninsured' => $value[4],
                'uninsured_moe' => $value[5],
            ), 
            array( 
                '%s', '%d', '%d', '%d', '%d', '%d'
            ) 
        );
        
        $lastid = $wpdb->insert_id;
        
        if (isset($lastid) && !empty($lastid)) {
            echo $lastid."<br>";
            //AGE CATEGORIES
            $wpdb->insert( 
                'age_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'age0to18',
                    'population'=>$value[8],
                    'insured' => $value[9],
                    'insured_moe' => $value[10],
                    'uninsured' => $value[11],
                    'uninsured_moe' => $value[12],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'age_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'age18to64',
                    'population'=>$value[15],
                    'insured' => $value[16],
                    'insured_moe' => $value[17],
                    'uninsured' => $value[18],
                    'uninsured_moe' => $value[19],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'age_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'age21to64',
                    'population'=>$value[22],
                    'insured' => $value[23],
                    'insured_moe' => $value[24],
                    'uninsured' => $value[25],
                    'uninsured_moe' => $value[26],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'age_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'age40to64',
                    'population'=>$value[29],
                    'insured' => $value[30],
                    'insured_moe' => $value[31],
                    'uninsured' => $value[32],
                    'uninsured_moe' => $value[33],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'age_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'age50to64',
                    'population'=>$value[36],
                    'insured' => $value[37],
                    'insured_moe' => $value[38],
                    'uninsured' => $value[39],
                    'uninsured_moe' => $value[40],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );

            //INCOME CATEGORIES
            $wpdb->insert( 
                'income_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'income0to138',
                    'population'=>$value[84],
                    'insured' => $value[85],
                    'insured_moe' => $value[86],
                    'uninsured' => $value[87],
                    'uninsured_moe' => $value[88],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'income_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'income0to200',
                    'population'=>$value[91],
                    'insured' => $value[92],
                    'insured_moe' => $value[93],
                    'uninsured' => $value[94],
                    'uninsured_moe' => $value[95],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'income_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'income0to250',
                    'population'=>$value[98],
                    'insured' => $value[99],
                    'insured_moe' => $value[100],
                    'uninsured' => $value[101],
                    'uninsured_moe' => $value[102],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'income_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'income0to400',
                    'population'=>$value[105],
                    'insured' => $value[106],
                    'insured_moe' => $value[107],
                    'uninsured' => $value[108],
                    'uninsured_moe' => $value[109],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'income_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'income138to400',
                    'population'=>$value[112],
                    'insured' => $value[113],
                    'insured_moe' => $value[114],
                    'uninsured' => $value[115],
                    'uninsured_moe' => $value[116],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );

            //RACES/ETHNICITIES CATEGORIES
            $wpdb->insert( 
                'races_ethnicities_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'whiteNotHispanic',
                    'population'=>$value[119],
                    'insured' => $value[120],
                    'insured_moe' => $value[121],
                    'uninsured' => $value[122],
                    'uninsured_moe' => $value[123],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );

            $wpdb->insert( 
                'races_ethnicities_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'blackNotHispanic',
                    'population'=>$value[126],
                    'insured' => $value[127],
                    'insured_moe' => $value[128],
                    'uninsured' => $value[129],
                    'uninsured_moe' => $value[130],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'races_ethnicities_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'hispanic',
                    'population'=>$value[133],
                    'insured' => $value[134],
                    'insured_moe' => $value[135],
                    'uninsured' => $value[136],
                    'uninsured_moe' => $value[137],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );

            //SEX CATEGORIES
            $wpdb->insert( 
                'sex_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'male',
                    'population'=>$value[43],
                    'insured' => $value[44],
                    'insured_moe' => $value[45],
                    'uninsured' => $value[46],
                    'uninsured_moe' => $value[47],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
            $wpdb->insert( 
                'sex_catgory', 
                array( 
                    'country_id' => $lastid,
                    'category' => 'male',
                    'population'=>$value[50],
                    'insured' => $value[51],
                    'insured_moe' => $value[52],
                    'uninsured' => $value[53],
                    'uninsured_moe' => $value[54],
                ), 
                array( 
                    '%d', '%s', '%d', '%d', '%d', '%d', '%d'
                ) 
            );
        }
        
    }
}