<?php /* Template Name: Reports */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	global $wpdb;
	$response = array();
	$userid = $_POST['userid'];
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id not exists';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	$role = implode(', ', $user_info->roles);
	if ($role != 'administrator') {
		$response['message'] = 'nopermission';
		$response['status'] = 'error';
		echo wp_send_json($response);		
	}

	$type = $_POST['type'];
	if ($type == 'heatmap') {
		//Check if country exists
		$results = $wpdb->get_results( "SELECT category_label, SUM(insured) as insured, SUM(uninsured) as uninsured FROM `age_catgory` GROUP BY `category`", ARRAY_A );
		if (isset($results) && !empty($results)) {
			$response['info'] = $results;

			$response['status'] = 'success';
			echo wp_send_json($response);
		}else{
			$response['info'] = 'No country exists with this id';
			$response['status'] = 'error';
			echo wp_send_json($response);
		}
	}else{
		$response['info'] = 'Bad request';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}
}