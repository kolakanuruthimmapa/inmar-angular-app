<?php /* Template Name: Admin data */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	global $wpdb;
	$response = array();
	$userid = $_POST['userid'];
	$type = $_POST['type'];
	$type_moe = $type.'_moe';
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id not exists';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	if ($type == 'userdashboard') {
		$countryid = $_POST['countryid'];
		//Check if country eixst or not
		$stats_info = $wpdb->get_results( "SELECT * FROM `country_info` WHERE id=$countryid AND user_id=$userid", ARRAY_A );
		if (isset($stats_info) && !empty($stats_info)) {
			$response['stats'] = $stats_info;

			$age_catgory = $wpdb->get_results( "SELECT * FROM `age_catgory` WHERE `country_id`=$countryid", ARRAY_A );
			if (isset($age_catgory) && !empty($age_catgory)) {
				$response['info']['age'] = $age_catgory;
			}

			$income_catgory = $wpdb->get_results( "SELECT * FROM `income_catgory` WHERE `country_id`=$countryid", ARRAY_A );
			if (isset($income_catgory) && !empty($income_catgory)) {
				$response['info']['income'] = $income_catgory;
			}

			$races_ethnicities = $wpdb->get_results( "SELECT * FROM `races_ethnicities_catgory` WHERE `country_id`=$countryid", ARRAY_A );
			if (isset($races_ethnicities) && !empty($races_ethnicities)) {
				$response['info']['races_ethnicities'] = $races_ethnicities;
			}

			$sex_catgory = $wpdb->get_results( "SELECT * FROM `sex_catgory` WHERE `country_id`=$countryid", ARRAY_A );
			if (isset($sex_catgory) && !empty($sex_catgory)) {
				$response['info']['sex'] = $sex_catgory;
			}
			$response['status'] = 'success';
			echo wp_send_json($response);exit;
		}else{
			$response['message'] = "There is country id exists with this country id $countryid or this country is not assigned to you.";
			$response['status'] = 'error';
			echo wp_send_json($response);	
		}
	}else{
		$role = implode(', ', $user_info->roles);
		if ($role != 'administrator') {
			$response['message'] = 'You can not add the country';
			$response['status'] = 'error';
			echo wp_send_json($response);
		}

		
		if ($type == 'dashboard') {
			$stats_info = $wpdb->get_results( "SELECT SUM(population) as population, SUM(insured) as insured, SUM(uninsured) as uninsured FROM `country_info`", ARRAY_A );
			if (isset($stats_info) && !empty($stats_info)) {
				$response['stats'] = $stats_info;
			}
			
			$info = $wpdb->get_results( "SELECT * FROM `country_info`", ARRAY_A );
			if (isset($info) && !empty($info)) {
				$response['info'] = $info;
			}		
		}else{
			$age_catgory = $wpdb->get_results( "SELECT id, category, category_label, SUM(`$type`) as $type, SUM(`$type_moe`) as $type_moe FROM `age_catgory` GROUP BY `category`", ARRAY_A );
			if (isset($age_catgory) && !empty($age_catgory)) {
				$response['info']['age'] = $age_catgory;
			}

			$income_catgory = $wpdb->get_results( "SELECT id, category, category_label, SUM(`$type`) as $type, SUM(`$type_moe`) as $type_moe FROM `income_catgory` GROUP BY `category`", ARRAY_A );
			if (isset($income_catgory) && !empty($income_catgory)) {
				$response['info']['income'] = $income_catgory;
			}

			$races_ethnicities_catgory = $wpdb->get_results( "SELECT id, category, category_label, SUM(`$type`) as $type, SUM(`$type_moe`) as $type_moe FROM `races_ethnicities_catgory` GROUP BY `category`", ARRAY_A );
			if (isset($races_ethnicities_catgory) && !empty($races_ethnicities_catgory)) {
				$response['info']['races_ethnicities'] = $races_ethnicities_catgory;
			}

			$sex_catgory = $wpdb->get_results( "SELECT id, category, category_label, SUM(`$type`) as $type, SUM(`$type_moe`) as $type_moe FROM `sex_catgory` GROUP BY `category`", ARRAY_A );
			if (isset($sex_catgory) && !empty($sex_catgory)) {
				$response['info']['sex'] = $sex_catgory;
			}
		}
		$response['status'] = 'success';
		echo wp_send_json($response);exit;
	}
}