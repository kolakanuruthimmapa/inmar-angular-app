<?php /* Template Name: Get Country Info */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	global $wpdb;
	$response = array();
	$userid = $_POST['userid'];
	$countryid = $_POST['countryid'];
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id not exists';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	$role = implode(', ', $user_info->roles);
	if ($role != 'administrator') {
		//Check user can access this country
		$country_user_id = $wpdb->get_var( "SELECT user_id FROM country_info WHERE id=$countryid" );
		if ($country_user_id != $userid) {
			$response['message'] = 'nopermission';
			$response['status'] = 'error';
			echo wp_send_json($response);
		}		
	}

	
	//Check if country exists
	$results = $wpdb->get_results( "SELECT * FROM country_info WHERE id=$countryid", ARRAY_A );
	if (isset($results) && !empty($results)) {
		$response['info'] = $results;

		//Get information with the categories
		$cat_array = array('age' => 'age_catgory', 'income' => 'income_catgory', 'races' => 'races_ethnicities_catgory', 'sex' => 'sex_catgory');
		$age_and_income = array();
		foreach ($cat_array as $key => $value) {
			$catgory_obj = $wpdb->get_results( "SELECT * FROM $value WHERE country_id=$countryid", ARRAY_A );
			if (isset($catgory_obj) && !empty($catgory_obj)) {
				$response[$key] = $catgory_obj;
			}
		}

		$response['status'] = 'success';
		echo wp_send_json($response);
	}else{
		$response['info'] = 'No country exists with this id';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}
}