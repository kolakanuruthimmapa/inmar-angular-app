<?php /* Template Name: Add Country */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();
	$userid = $_POST['userid'];
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id is required';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	$countryname = $_POST['countryname'];
	$population = $_POST['population'];
	$insured = $_POST['insured'];
	$uninsured = $_POST['uninsured'];
	$insuredmoe = $_POST['insuredmoe'];
	$uninsuredmoe = $_POST['uninsuredmoe'];

	$edit_countryid = $_POST['countryid'];

	$role = implode(', ', $user_info->roles);
	if ($role != 'administrator') {
		//Check user can access this country
		$country_user_id = $wpdb->get_var( "SELECT user_id FROM country_info WHERE id=$edit_countryid" );
		if ($country_user_id != $userid) {
			$response['message'] = 'You can not add the country';
			$response['status'] = 'error';
			echo wp_send_json($response);
		}
	}

	
	
    //echo wp_send_json($_POST);


	if (isset($countryname) && !empty($countryname)) {
		global $wpdb;

		if (isset($edit_countryid) && $edit_countryid > 0) {
			$exist_country_id = $wpdb->get_var( "SELECT id FROM country_info WHERE id = $edit_countryid" );
			if (isset($exist_country_id) && !empty($exist_country_id)) {
				$wpdb->update( 
					'country_info', 
					array( 
						'name' => $countryname, 
		                'population' => $population,
		                'insured' => $insured,
		                'insured_moe' => $insuredmoe,
		                'uninsured' => $uninsured,
		                'uninsured_moe' => $uninsuredmoe,
					), 
					array( 'id' => $edit_countryid ), 
					array( '%s', '%d', '%d', '%d', '%d', '%d' ), 
					array( '%d' ) 
				);

				$lastid = $edit_countryid;
			}else{
				$response['message'] = 'Country is not exist';
				$response['status'] = 'error';
				echo wp_send_json($response);exit;
			}
		}else{
			//Check if country is exists
			$exist_country_id = $wpdb->get_var( "SELECT id FROM country_info WHERE name LIKE %$countryname%" );
			if (isset($exist_country_id) && !empty($exist_country_id)) {
				$response['message'] = 'Country name is already exist';
				$response['status'] = 'error';
				echo wp_send_json($response);exit;
			}

			//Insert country infrmation
	        $wpdb->insert( 
	            'country_info', 
	            array( 
	                'name' => $countryname, 
	                'population' => $population,
	                'insured' => $insured,
	                'insured_moe' => $insuredmoe,
	                'uninsured' => $uninsured,
	                'uninsured_moe' => $uninsuredmoe,
	            ), 
	            array( 
	                '%s', '%d', '%d', '%d', '%d', '%d'
	            ) 
	        );

	        $lastid = $wpdb->insert_id;
		}

		
        if (isset($lastid) && !empty($lastid)) {
        	//var_dump($_POST);
        	foreach($_POST as $key => $value){
        		$table_name = '';
        		$category_name = '';
			    $category_label = '';
			    if (strpos($key, 'age') !== false) {		
			    	$table_name = 'age_catgory';	    	
			    	switch ($key) {
			    		case 'age0to18':
			    			$category_name = 'age0to18';
			    			$category_label = '0 to 18';
			    			break;
			    		case 'age18to64':
			    			$category_name = 'age18to64';
			    			$category_label = '18 to 64';
			    			break;
			    		case 'age21to64':
			    			$category_name = 'age21to64';
			    			$category_label = '21 to 64';
			    			break;
			    		case 'age40to64':
			    			$category_name = 'age40to64';
			    			$category_label = '40 to 64';
			    			break;
			    		case 'age50to64':
			    			$category_name = 'age50to64';
			    			$category_label = '50 to 64';
			    			break;
			    	}
			    }elseif (strpos($key, 'income') !== false) {
			    	$table_name = 'income_catgory';
			    	switch ($key) {
			    		case 'income0to138':
			    			$category_name = 'income0to138';
			    			$category_label = '0 to 138';
			    			break;
			    		case 'income0to200':
			    			$category_name = 'income0to200';
			    			$category_label = '0 to 200';
			    			break;
			    		case 'income0to250':
			    			$category_name = 'income0to250';
			    			$category_label = '0 to 250';
			    			break;
			    		case 'income0to400':
			    			$category_name = 'income0to400';
			    			$category_label = '0 to 400';
			    			break;
			    		case 'income138to400':
			    			$category_name = 'income138to400';
			    			$category_label = '138 to 400';
			    			break;
			    	}
			    }elseif ($key == 'male') {
			    	$table_name = 'sex_catgory';
			    	$category_name = 'male';
			    	$category_label = 'Male';
			    }elseif ($key == 'female') {
			    	$table_name = 'sex_catgory';
			    	$category_name = 'female';
			    	$category_label = 'Female';
			    }elseif ($key == 'whiteNotHispanic') {
			    	$table_name = 'races_ethnicities_catgory';
			    	$category_name = 'whiteNotHispanic';
			    	$category_label = 'White Not Hispanic';
			    }elseif ($key == 'blackNotHispanic') {
			    	$table_name = 'races_ethnicities_catgory';
			    	$category_name = 'blackNotHispanic';
			    	$category_label = 'Black Not Hispanic';
			    }elseif ($key == 'hispanic') {
			    	$table_name = 'races_ethnicities_catgory';
			    	$category_name = 'hispanic';
			    	$category_label = 'Hispanic';
			    }
			    //echo $table_name.'@@'.$category_name.'@@'.$category_label."#####\n";
			    if (!empty($table_name) && !empty($category_name) && !empty($category_label)) {
			    	if (isset($edit_countryid) && $edit_countryid > 0) {
			    		$wpdb->update( 
							$table_name,
							array( 
			                    'population'=>$value['population'],
			                    'insured' => $value['insured'],
			                    'insured_moe' => $value['insuredMoe'],
			                    'uninsured' => $value['uninsured'],
			                    'uninsured_moe' => $value['uninsuredMoe'],
							), 
							array( 'country_id' => $edit_countryid, 'category' => $category_name), 
							array( '%d', '%s', '%s', '%d', '%d', '%d', '%d', '%d' ), 
							array( '%d', '%s' ) 
						);
			    	}else{
					    $wpdb->insert( 
			                $table_name, 
			                array( 
			                    'country_id' => $lastid,
			                    'category' => $category_name,
			                    'category_label' => $category_label,
			                    'population'=>$value['population'],
			                    'insured' => $value['insured'],
			                    'insured_moe' => $value['insuredMoe'],
			                    'uninsured' => $value['uninsured'],
			                    'uninsured_moe' => $value['uninsuredMoe'],
			                ), 
			                array( 
			                    '%d', '%s', '%s', '%d', '%d', '%d', '%d', '%d'
			                ) 
			            );
					}
				}
			}

			$response['message'] = 'Country created';
			$response['status'] = 'success';
			echo wp_send_json($response);
        }
	}else{
		$response['message'] = 'Country name is required';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}
}