<?php /* Template Name: Get User Info */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();

	$fullname = $_POST['fullname'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$countryname = $_POST['country'];
	$pincode = $_POST['pincode'];
	$phone = $_POST['phone'];

	$type = $_POST['type'];
	$edit_user_id = $_POST['userid'];
	if ($type == 'edit') {
		$user_id = wp_update_user( array( 'ID' => $edit_user_id, 'first_name' => $fullname ) );
		if ( is_wp_error( $user_id ) ) {
			$response['message'] = "There was an error, probably that user does not exist.";
			$response['status'] = 'error';
			echo wp_send_json($response);exit;
		} else {
			update_user_meta($user_id, 'city', $city);
		    update_user_meta($user_id, 'state', $state);
		    update_user_meta($user_id, 'country', $country);
		    update_user_meta($user_id, 'pincode', $pincode);
		    update_user_meta($user_id, 'phone', $phone);
		}
		$response['message'] = 'User updated';
		$response['status'] = 'success';
		echo wp_send_json($response);
	}else{
		$user_obj = get_user_by('ID', $edit_user_id);
		if (isset($user_obj) && !empty($user_obj)) {
			$user_country_name = $wpdb->get_var( "SELECT name FROM country_info WHERE user_id = $edit_user_id" );
			if (!isset($user_country_name) || empty($user_country_name)) {
				$user_country_name = '';
			}
			$response = array(
				'id' => $edit_user_id,
				'email' => $user_obj->user_email,
				'fullname' => $user_obj->first_name,
				'city' => get_user_meta($edit_user_id, 'city', true),
				'state' => get_user_meta($edit_user_id, 'state', true),
				'countryname' => $user_country_name,
				'pincode' => get_user_meta($edit_user_id, 'pincode', true),
				'phone'	=> get_user_meta($edit_user_id, 'phone', true)
			);
			$response['status'] = 'success';
		}else{
			$response['message'] = "There was an error, probably that user does not exist.";
			$response['status'] = 'error';
		}
		echo wp_send_json($response);
	}
}