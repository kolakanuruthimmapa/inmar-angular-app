<?php /* Template Name: Login */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();
	if (isset($_POST['username']) && !empty($_POST['username'])) {
		$username = $_POST['username'];
	}else{
		$response['username'] = 'Username required';
	}

	if (isset($_POST['password']) && !empty($_POST['password'])) {
		$password = $_POST['password'];
	}else{
		$response['password'] = 'Password required';
	}

	if (count($response) == 0) {
		global $wpdb;
		//We shall SQL escape all inputs
		$username = $wpdb->escape($username);
		$password = $wpdb->escape($password);
		$remember = $wpdb->escape($_POST['rememberme']);
	 
		if($remember) {
			$remember = "true";
		}else {
			$remember = "false";
		}
	 
		$login_data = array();
		$login_data['user_login'] = $username;
		$login_data['user_password'] = $password;
		$login_data['remember'] = $remember;
		$user_verify = wp_signon( $login_data, false ); 
	 
		if ( is_wp_error($user_verify) ) {
			$response['status'] = 'error';
			$response['message'] = 'Invalid username or password';
		   // Note, I have created a page called "Error" that is a child of the login page to handle errors. This can be anything, but it seemed a good way to me to handle errors.
		 } else {	
		   	//Valid User
		   	$user_id = $user_verify->ID;
		   	$user_meta=get_userdata($user_id); 
		   	$user_roles=$user_meta->roles; 
		   	if (in_array("subscriber", $user_roles)){
		   		$role = 'subscriber';
		   		$user_country_id = $wpdb->get_var( "SELECT id FROM country_info WHERE user_id = $user_id" );
		   	}else{
		   		$role = 'admin';
		   		$user_country_id = 0;
		   	}
		 	$response['status'] = 'success';


		 	

		 	$response['userinfo'] = array('id' => $user_id, 'email'=>$user_verify->user_email, 'name' => $user_verify->display_name, 'role' => $role, 'user_country_id' => $user_country_id);
		 }
	}else{
		$response['message'] = 'Please fill username and password';
		$response['status'] = 'error';
	}
	
    echo wp_send_json($response);
}