<?php /* Template Name: Get Country List */ ?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'),TRUE);
	$response = array();
	$userid = $_POST['userid'];
	//Check if current is admin or not
	$user_info = get_user_by( 'ID', $userid );
	if (!isset($user_info) || empty($user_info)) {
		$response['message'] = 'User id not exists';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	$role = implode(', ', $user_info->roles);
	if ($role != 'administrator') {
		$response['message'] = 'You can not add the country';
		$response['status'] = 'error';
		echo wp_send_json($response);
	}

	global $wpdb;
	$results = $wpdb->get_results( "SELECT * FROM country_info", ARRAY_A );
	if (isset($results) && !empty($results)) {
		$response['countries'] = $results;
	}else{
		$response['countries'] = array();
	}

	$response['status'] = 'success';
	echo wp_send_json($response);
}